import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YoutubeOverlayComponent } from './components/youtube-overlay/youtube-overlay.component';
import { SafePipe } from './pipes/safe/safe.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [YoutubeOverlayComponent, SafePipe],
  exports: [YoutubeOverlayComponent]
})
export class YoutubeOverlayModule { }
