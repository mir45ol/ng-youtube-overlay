import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Color } from '../../enums/color.enum';

@Component({
  selector: 'uc-youtube-overlay',
  templateUrl: './youtube-overlay.component.html',
  styleUrls: ['./youtube-overlay.component.scss']
})
export class YoutubeOverlayComponent {

  @Input('privacy-enhanced-mode') privacyEnhancedMode: boolean;
  @Input('video-id') videoId: string;
  @Input() autoplay: boolean;
  @Input('cc-lang-pref') ccLangPref: string // ISO 639-1 two-letter language code. http://www.loc.gov/standards/iso639-2/php/code_list.php
  @Input('cc-load-policy') ccLoadPolicy: number; // TODO: Write an enum for this.
  @Input() color: Color;
  @Input() controls: boolean;
  @Input() disablekb: boolean;
  @Input() enablejsapi: boolean;
  @Input() end: number;
  @Input() fs: boolean;
  @Input() hl: string; // ISO 639-1 two-letter language code. http://www.loc.gov/standards/iso639-2/php/code_list.php
  @Input('iv-load-policy') ivLoadPolicy: number; // TODO: Write an enum for this.
  @Input() list; // TODO: Figure this out.
  @Input('list-type') listType;
  @Input() loop: boolean;
  @Input() modestbranding: boolean;
  @Input() origin: string; // TODO: Sanitize this?
  @Input() playlist; // TODO: Figure this out.
  @Input() playsinline: boolean;
  @Input() rel: boolean;
  @Input() showinfo: boolean;
  @Input() start: number;
  @Input('widget-referrer') widgetReferrer: string; // TODO: Sanitize this?

  fadeIn: boolean = false;
  fadeOut: boolean = false;

  private _videoUrl: string;
  private _show: boolean;

  @Input() set show(show: boolean) {
    this.fadeIn = show;
    this.fadeOut = !show;

    if (show) {
      this._show = show;
      this.showChange.emit(show);
    } else {
      setTimeout(() => {
        this._show = show;
        this.showChange.emit(show);
      }, 300);
    }
  }

  @Output() showChange: EventEmitter<boolean> = new EventEmitter();

  get videoUrl(): string {
    return this._videoUrl;
  }

  get show(): boolean {
    return this._show;
  }

  $hide() {
    this.show = false;
  }
}
